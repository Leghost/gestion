import React from 'react';

const Home = React.lazy(() => {
  return import('./views/Home/Home');
});

const Pricing = React.lazy(() => {
  return import('./views/Pricing/Pricing');
});

const Faq = React.lazy(() => {
  return import('./views/Faq/FAQ');
});

const Contact = React.lazy(() => {
  return import('./views/Contact/Contact');
});

const routes = [
  { path: '/', component: Home },
  { path: '/pricing', component: Pricing },
  { path: '/faq', component: Faq },
  { path: '/Contact', component: Contact }


];


export default routes;
