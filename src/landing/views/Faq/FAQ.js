import React from "react"
import { Row, Col } from "reactstrap"
import QuestionsSearch from "./QuestionsSearch"

import "../../../assets/scss/pages/faq.scss"


class Faq extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.scrollNavigation = this.scrollNavigation.bind(this);

  }

  componentDidMount() {
    document.body.classList = "";
    window.addEventListener("scroll", this.scrollNavigation, true);
  }

  // Make sure to remove the DOM listener when the component is unmounted.
  componentWillUnmount() {
    window.removeEventListener("scroll", this.scrollNavigation, true);
  }

  scrollNavigation = () => {
    const doc = document.documentElement;
    const top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    if (top > 80) {
      document.getElementById('topnav').classList.add('nav-sticky');
    } else {
      document.getElementById('topnav').classList.remove('nav-sticky');
    }
  };


  render() {
    return (
      <React.Fragment>
        <Row className="justify-content-center mt-100">
          <Col sm="12">
            <QuestionsSearch />
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

export default Faq
