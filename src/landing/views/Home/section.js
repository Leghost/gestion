import React, { Component } from 'react';
import {
  Container, Row, Col, Card,
  CardHeader, CardTitle, CardBody, Button, Toast, ToastHeader, ToastBody, CardFooter
} from 'reactstrap';

import Wizard from "./WizardComponent"
import tete from "../Home/tete.jpg"
import tata from "../Home/tata.png"
import toto from "../Home/toto.png"
import titi from "../Home/titi.jpg"
import Akil from "../Home/Akilcab.png"
import Swiper from "react-id-swiper"
import "swiper/css/swiper.css"
import Carousel from 'react-bootstrap/Carousel'
import soro from "../Home/soro.jpg"
import tro from "../Home/tro.jpg"
import camara from "../Home/camara.jpg"
import flan from "../Home/flan.jpg"
import yapi from "../Home/yapi.jpg"




// import images
import Ged1 from '../../../assets/img/user/Ged1.JPG'
//import Dashboard from './../../../app/views/dashboard/Dashboard';//


const swiperParams = {
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev"
  },
  breakpoints: {
    1800: {
      slidesPerView: 5,
      spaceBetween: 10
    },
    1600: {
      slidesPerView: 4,
      spaceBetween: 10
    },
    1200: {
      slidesPerView: 3,
      spaceBetween: 10
    },
    1000: {
      slidesPerView: 3,
      spaceBetween: 10
    },
    800: {
      slidesPerView: 2,
      spaceBetween: 10
    },
    600: {
      slidesPerView: 1,
      spaceBetween: 10
    }
  }
}
class Section extends Component {
  state = {
    activeTab: "1",
  }
  state = {
    steps: [
      {
        title: 1,
        content: <Row className="justify-content-center mb-5">
          <Col md="6" sm="8">
            <div className="p-5 bg-primary my-2 rounded">
              <Toast className="p-6 justify-content-center ml-5">
                <ToastHeader><h3>INSCRIVEZ-VOUS</h3></ToastHeader>
                <ToastBody >
                  <h5>Inscrivez-vous en tant qu'entreprise ou particulier.</h5>
                  <Button className="justify-content-center ml-lg-5" color="primary">Inscription</Button>
                </ToastBody>
              </Toast>
            </div>
          </Col>
        </Row>
      }, {
        title: 2,
        content: <Row className="justify-content-center mb-5">
          <Col md="6" sm="8">
            <div className="p-5 bg-info my-2 rounded">
              <Toast className="p-6 justify-content-center ml-5">
                <ToastHeader><h3>CONNECTEZ-VOUS</h3></ToastHeader>
                <ToastBody>
                  <h5>Entrez vos identifiants pour acceder a votre Ged.</h5>
                  <Button className="justify-content-center ml-lg-5" color="info">Connexion</Button>
                </ToastBody>
              </Toast>
            </div>
          </Col>
        </Row>
      }, {
        title: 3,
        content: <Row className="justify-content-center mb-5">
          <Col md="6" sm="8">
            <div className="p-5 bg-danger my-2 rounded">
              <Toast className="p-6 justify-content-center ml-5 ">
                <ToastHeader ><h3>GERER VOS DOCUMENTS</h3></ToastHeader>
                <ToastBody >
                  <h5>Acceder a votre Dashboard et commencer a gerer vos fichiers.</h5>
                  <Button className="justify-content-center ml-lg-5" color="danger">Inscription</Button>
                </ToastBody>
              </Toast>
            </div>
          </Col>
        </Row>
      }
    ]
  };


  render() {
    const { steps } = this.state
    return (
      <React.Fragment>
        <section className="bg-half-170 d-table w-100 mb-100" id="home">
          <Container>
            <Row className="align-items-center">
              <Col lg={6} md={6}>
                <div className="title-heading mt-4">
                  <h1 className="heading mb-1">La Gestion de vos document vous prend assez de temps?</h1>
                  <h3>Ne vous inquitez pas , nous avons la Solution:  AKILGED</h3>
                  <p className="para-desc text-muted"></p>
                  <p className="para-desc text-muted">It has survived
                  not only five centuries, but also the leap into electronic typesetting, remaining essentially
                  unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem
                  Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including
                  versions of Lorem Ipsum.
                  </p>
                </div>
              </Col>
              <Col lg={6} md={6} className="mt-4 pt-2 mt-sm-0 pt-sm-0">
                <img width="760" height="400" src={Ged1} alt="" />
              </Col>
            </Row>
          </Container>
        </section>
        <div >
          <Row className="justify-content-center mb-60">
            <h1>
              AkilGed vous propose plusieurs avantages
            </h1>
          </Row>
          <Carousel className="mb-60">
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={toto}
                alt="First slide"
                height="500px"
              />
              <Carousel.Caption>
                <Row className="align-items-end">
                  <Col lg="12" md="8" sm="2">
                    <Card color="succes">
                      <h2>Réduction des coûts d’exploitation</h2>
                      <h4>
                        Plus besoin d’armoires ni de classeurs, à vous le bureau spacieux ! La GED permet de réduire votre espace de stockage et donc les coûts itinérants.
                        De plus, il ne vous sera plus nécessaire de réaliser plusieurs copies d’un même document puisqu’une version numérique sera désormais stockée dans l’armoire numérique de l’entreprise et accessible à tous les salariés autorisés,
                        vous réduisez donc également vos coûts d’impression.
                      </h4>
                    </Card>
                  </Col>
                </Row>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={tata}
                alt="Third slide"
                height="500px"
              />
              <Carousel.Caption>
                <Row className="align-items-end">
                  <Col lg="12" md="8" sm="2">
                    <Card color="succes">
                      <h2>Réduction des coûts d’exploitation</h2>
                      <h4>
                        Plus besoin d’armoires ni de classeurs, à vous le bureau spacieux ! La GED permet de réduire votre espace de stockage et donc les coûts itinérants.
                        De plus, il ne vous sera plus nécessaire de réaliser plusieurs copies d’un même document puisqu’une version numérique sera désormais stockée dans l’armoire numérique de l’entreprise et accessible à tous les salariés autorisés,
                        vous réduisez donc également vos coûts d’impression.
                        </h4>
                    </Card>
                  </Col>
                </Row>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={titi}
                alt="Third slide"
                height="500px"
              />
              <Carousel.Caption>
                <Row className="align-items-end">
                  <Col lg="12" md="8" sm="2">
                    <Card color="succes">
                      <h2>Réduction des coûts d’exploitation</h2>
                      <h4>
                        Plus besoin d’armoires ni de classeurs, à vous le bureau spacieux ! La GED permet de réduire votre espace de stockage et donc les coûts itinérants.
                        De plus, il ne vous sera plus nécessaire de réaliser plusieurs copies d’un même document puisqu’une version numérique sera désormais stockée dans l’armoire numérique de l’entreprise et accessible à tous les salariés autorisés,
                        vous réduisez donc également vos coûts d’impression.
                        </h4>
                    </Card>
                  </Col>
                </Row>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={titi}
                alt="Third slide"
                height="500px"
              />
              <Carousel.Caption>
                <Row className="align-items-end">
                  <Col lg="12" md="8" sm="2">
                    <Card color="succes">
                      <h2>Réduction des coûts d’exploitation</h2>
                      <h4>
                        Plus besoin d’armoires ni de classeurs, à vous le bureau spacieux ! La GED permet de réduire votre espace de stockage et donc les coûts itinérants.
                        De plus, il ne vous sera plus nécessaire de réaliser plusieurs copies d’un même document puisqu’une version numérique sera désormais stockée dans l’armoire numérique de l’entreprise et accessible à tous les salariés autorisés,
                        vous réduisez donc également vos coûts d’impression.
                        </h4>
                    </Card>
                  </Col>
                </Row>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={titi}
                alt="Third slide"
                height="500px"
              />
              <Carousel.Caption>
                <Row className="align-items-end">
                  <Col lg="12" md="8" sm="2">
                    <Card color="succes">
                      <h2>Réduction des coûts d’exploitation</h2>
                      <h4>
                        Plus besoin d’armoires ni de classeurs, à vous le bureau spacieux ! La GED permet de réduire votre espace de stockage et donc les coûts itinérants.
                        De plus, il ne vous sera plus nécessaire de réaliser plusieurs copies d’un même document puisqu’une version numérique sera désormais stockée dans l’armoire numérique de l’entreprise et accessible à tous les salariés autorisés,
                        vous réduisez donc également vos coûts d’impression.
                        </h4>
                    </Card>
                  </Col>
                </Row>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
          <Row className="justify-content-center mb-60"><h1>Comment ca marche?</h1></Row>
          <Row className="justify-content-center mb-60" >
            <Col lg="10" md="6" sm="8">
              <Card >
                <CardHeader>
                  <CardTitle>PROCESSUS</CardTitle>
                </CardHeader>
                <CardBody>
                  <Wizard className="justify-content-center"
                    enableAllSteps
                    steps={steps}
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row className="justify-content-center mb-60"><h1>Temoignages</h1></Row>
          <Card color="info" >
            <Row className="justify-content-center mb-60" >
              <Col className="details-page-swiper text-center mt-5" sm="12">
                <Swiper {...swiperParams}>
                  <Col lg="4" md="6" sm="8">
                    <Card >
                      <CardHeader className="justify-content-center mb-4" >
                        <span>
                          <img
                            src={tete}
                            className="round"
                            height="60"
                            width="60"
                            alt="avatar"
                          />
                        </span>
                      </CardHeader>
                      <CardBody>
                        <b className="font-size-large">``</b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, perspiciatis.
                        Minima cum delectus quis quaerat eum repellat quidem
                        ducimus autem culpa! Soluta officia quidem voluptate, illo harum libero dolorum natus!
                      </CardBody>
                      <CardFooter>
                        <b>Bakayoko cheick</b>
                        <h5>Professeur titulaire en intelligence artificielle</h5>
                      </CardFooter>
                    </Card>
                  </Col>
                  <Col lg="4" md="6" sm="8">
                    <Card >
                      <CardHeader className="justify-content-center mb-4" >
                        <span>
                          <img
                            src={tro}
                            className="round"
                            height="60"
                            width="60"
                            alt="avatar"
                          />
                        </span>
                      </CardHeader>
                      <CardBody>
                        <b className="font-size-large">``</b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, perspiciatis.
                        Minima cum delectus quis quaerat eum repellat quidem
                        ducimus autem culpa! Soluta officia quidem voluptate, illo harum libero dolorum natus!
                      </CardBody>
                      <CardFooter>
                        <b>Tro Emannuel</b>
                        <h5>PDG d'Emmaniel Infographie</h5>
                      </CardFooter>
                    </Card>
                  </Col>
                  <Col lg="4" md="6" sm="8">
                    <Card >
                      <CardHeader className="justify-content-center mb-4" >
                        <span>
                          <img
                            src={soro}
                            className="round"
                            height="60"
                            width="60"
                            alt="avatar"
                          />
                        </span>
                      </CardHeader>
                      <CardBody>
                        <b className="font-size-large">``</b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, perspiciatis.
                        Minima cum delectus quis quaerat eum repellat quidem
                        ducimus autem culpa! Soluta officia quidem voluptate, illo harum libero dolorum natus!
                      </CardBody>
                      <CardFooter>
                        <b>Soro Kazana</b>
                        <h5>Professeur titulaire en Mathematique</h5>
                      </CardFooter>
                    </Card>
                  </Col>
                  <Col lg="4" md="6" sm="8">
                    <Card >
                      <CardHeader className="justify-content-center mb-4" >
                        <span>
                          <img
                            src={yapi}
                            className="round"
                            height="60"
                            width="60"
                            alt="avatar"
                          />
                        </span>
                      </CardHeader>
                      <CardBody>
                        <b className="font-size-large">``</b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, perspiciatis.
                        Minima cum delectus quis quaerat eum repellat quidem
                        ducimus autem culpa! Soluta officia quidem voluptate, illo harum libero dolorum natus!
                      </CardBody>
                      <CardFooter>
                        <b>Yapi cyriaque Fredi</b>
                        <h5>Colonel et Big data scientist</h5>
                      </CardFooter>
                    </Card>
                  </Col>
                  <Col lg="4" md="6" sm="8">
                    <Card >
                      <CardHeader className="justify-content-center mb-4" >
                        <span>
                          <img
                            src={camara}
                            className="round"
                            height="60"
                            width="60"
                            alt="avatar"
                          />
                        </span>
                      </CardHeader>
                      <CardBody>
                        <b className="font-size-large">``</b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, perspiciatis.
                        Minima cum delectus quis quaerat eum repellat quidem
                        ducimus autem culpa! Soluta officia quidem voluptate, illo harum libero dolorum natus!
                      </CardBody>
                      <CardFooter>
                        <b>Camara Moussa</b>
                        <h5>Financier a la Bad</h5>
                      </CardFooter>
                    </Card>
                  </Col>
                  <Col lg="4" md="6" sm="8">
                    <Card >
                      <CardHeader className="justify-content-center mb-4" >
                        <span>
                          <img
                            src={flan}
                            className="round"
                            height="60"
                            width="60"
                            alt="avatar"
                          />
                        </span>
                      </CardHeader>
                      <CardBody>
                        <b className="font-size-large">``</b>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, perspiciatis.
                        Minima cum delectus quis quaerat eum repellat quidem
                        ducimus autem culpa! Soluta officia quidem voluptate, illo harum libero dolorum natus!
                      </CardBody>
                      <CardFooter>
                        <b>Flan Jean-Charles</b>
                        <h5>PDG de FL tech</h5>
                      </CardFooter>
                    </Card>
                  </Col>
                </Swiper>
              </Col>
            </Row>
          </Card>
          <div>
            <Row className="justify-content-center mb-60"><h1>Ces entreprises nous font confiance</h1></Row>
            <Row className="justify-content-center mb-60">
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
            </Row>
            <Row className="justify-content-center mb-60">
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
              <Col>
                <img
                  src={Akil}
                  height="150"
                  width="150"
                  alt="avatar"
                />

              </Col>
            </Row>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Section;
