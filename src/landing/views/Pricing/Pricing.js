// React Basic and Bootstrap
import React, { Component } from 'react';
import { Row, Card, Col, ListGroup, ListGroupItem, Button } from 'reactstrap';


// Import Generic Components

class Pricing extends Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.scrollNavigation = this.scrollNavigation.bind(this);

  }

  componentDidMount() {
    document.body.classList = "";
    window.addEventListener("scroll", this.scrollNavigation, true);
  }

  // Make sure to remove the DOM listener when the component is unmounted.
  componentWillUnmount() {
    window.removeEventListener("scroll", this.scrollNavigation, true);
  }

  scrollNavigation = () => {
    const doc = document.documentElement;
    const top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    if (top > 80) {
      document.getElementById('topnav').classList.add('nav-sticky');
    } else {
      document.getElementById('topnav').classList.remove('nav-sticky');
    }
  };


  render() {
    return (
      <React.Fragment>
        {/* Home Section */}
        <div className="bg-half-170">

          <Row className="justify-content-center mb-60">
            <Col lg="9" md="6" sm="8">
              <h1>
                Souscrivez a nos packs !
              </h1>
              <h3>
                Akilged vous offre ses tarifications pour la bonne gestion de vos documents en ligne.
              </h3>
            </Col>
          </Row>

          <Row className="justify-content-center">
            <Col lg="3" md="6" sm="8">
              <Card>
                <ListGroup>
                  <ListGroupItem className="bg-gradient-success">
                    <h1>Basic</h1>
                  </ListGroupItem>
                  <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
                  <ListGroupItem>Morbi leo risus</ListGroupItem>
                  <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
                  <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
                  <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
                  <ListGroupItem className="bg-gradient-success" >
                    <Button outline color="dark">
                      souscrire
                    </Button>
                  </ListGroupItem>
                </ListGroup>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="8">
              <Card>
                <ListGroup>
                  <ListGroupItem className="bg-gradient-primary">
                    <h1>Standard</h1>
                  </ListGroupItem>
                  <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
                  <ListGroupItem>Morbi leo risus</ListGroupItem>
                  <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
                  <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
                  <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
                  <ListGroupItem className="bg-gradient-primary" >
                    <Button outline color="dark">
                      souscrire
                    </Button>
                  </ListGroupItem>
                </ListGroup>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="8">
              <Card>
                <ListGroup>
                  <ListGroupItem className="bg-gradient-info  ">
                    <h1>Premium</h1>
                  </ListGroupItem>
                  <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
                  <ListGroupItem>Morbi leo risus</ListGroupItem>
                  <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
                  <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
                  <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
                  <ListGroupItem className="bg-gradient-info" >
                    <Button outline color="dark">
                      souscrire
                    </Button>
                  </ListGroupItem>
                </ListGroup>
              </Card>
            </Col>

          </Row>
        </div>

      </React.Fragment>
    );
  }
}

export default Pricing;

