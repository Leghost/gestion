import React from "react"
import * as Icon from "react-feather"
const navigationConfig = [
  {
    id: "dashboard",
    title: "Tableau de bords",
    type: "item",
    icon: <Icon.Home size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard/apps/:appId/dashboard"
  },

  {
    id: "GestionFichier",
    title: "Gestion des Fichiers",
    type: "item",
    icon: <Icon.Folder size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard/GestionFile"

  },


  {
    id: "GestionProfil",
    title: "Gestion des acces",
    type: "item",
    icon: <Icon.User size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard/apps/:appId/gestionprofil"
  },



  {
    id: "Corbeille",
    title: "Corbeille",
    type: "item",
    icon: <Icon.Archive size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard/Corbeille",
  },













];

export default navigationConfig
