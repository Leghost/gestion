
import { history } from "../../../../../history"
import axios from "axios"


export const loginWithJWT = user => {
  return dispatch => {
    axios
      .post("/api/authenticate/login/user", {
        email: user.email,
        password: user.password
      })
      .then(response => {
        let loggedInUser;

        if (response.data) {
          loggedInUser = response.data.user;
          localStorage.setItem('access_token', response.data.accessToken);
          dispatch({
            type: "LOGIN_WITH_JWT",
            isAuthenticated: true,
            payload: { loggedInUser, loggedInWith: "jt" },
          });
          history.push("/dashboard/espace")
        }
      })
      .catch(err => console.log(err))
  }
};

export const logoutWithJWT = () => {
  return dispatch => {
    dispatch({ type: "LOGOUT_WITH_JWT", payload: {} });
    history.push("/pages/login")
  }
};
