import React from "react";
import {Nav, NavItem, NavLink, TabContent, TabPane, Row, Col} from "reactstrap";
import classnames from "classnames";
import Register from "./register/Register";
import Login from "./login/Login";
import Imgauth from "../../../assets/img/pages/login.png"


class Auth extends React.Component {
  state = {
    active: "1"
  };

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({active: tab})
    }
  };

  render() {
    return (
        <div className={'mt-5'}>
          <Row className={'mr-1 ml-1'}>
            <Col lg={7} className={'mt-2'}>
              <Row className="align-items-center">
                <Col lg="5">
                  <div className="title-heading mt-4">
                    <img src={Imgauth} alt="connexion" height="400px" width="700"/>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col lg={4}>
              <Nav tabs className="justify-content-center mt-3">
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.active === "1"
                    })}
                    onClick={() => {
                      this.toggle("1")
                    }}
                  >
                    Compte AkilGed
                  </NavLink>
                </NavItem>

              </Nav>
              <TabContent activeTab={this.state.active}>
                <TabPane tabId="1">
                  <Login/>
                </TabPane>
                <TabPane tabId="2">
                  <Register/>
                </TabPane>
              </TabContent>
            </Col>
          </Row>
        </div>
    )
  }
}

export default Auth
