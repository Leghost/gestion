import React from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Form,
  Button,
  Label
} from "reactstrap"
import { Link } from "react-router-dom"

import Checkbox from "../../../@vuexy/checkbox/CheckboxesVuexy"
import { Check, Mail, Lock } from "react-feather"
import { connect } from "react-redux"
import { loginWithJWT } from "../../../@core/redux/actions/auth/loginActions"

class Login extends React.Component {

  state = {
    email: "demo@demo.com",
    password: "demodemo",
    remember: false
  };

  handleLogin = (e) => {
    e.preventDefault();
    this.props.loginWithJWT(this.state)
  };

  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>Compte AkilGED</CardTitle>
        </CardHeader>
        <CardBody>
          <Form onSubmit={this.handleLogin}>
            <Row>

              <Col sm="12">
                <Label for="UserIcons">Nom d'utilisateur</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="email"
                    name="Email"
                    id="UserIcons"
                    placeholder="Nom d'utilisateur"
                  />
                  <div className="form-control-position">
                    <Mail size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="IconsPassword">Mot de passe</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="password"
                    name="password"
                    id="IconsPassword"
                    placeholder="Mot de passe"
                  />
                  <div className="form-control-position">
                    <Lock size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup className="d-flex justify-content-between align-items-center">
                <Checkbox
                  color="primary"
                  icon={<Check className="vx-icon" size={16} />}
                  label="Rester connecter"
                  defaultChecked={false}
                  onChange={this.handleRemember}
                />
                <div className="float-right">
                  <Link to="/dashboard/Motdepasseoublie">Mot de passe oublie?</Link>
                </div>
              </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup className="has-icon-left position-relative">
                  <Button.Ripple
                    color="primary"
                    type="submit"
                    className="mr-1 mb-1"
                  >
                    Se connecter
                  </Button.Ripple>
                </FormGroup>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
    )
  }
}

const mapStateToProps = state => {
  return {
    values: state.auth.login
  }
};

export default connect(mapStateToProps, { loginWithJWT })(Login)
