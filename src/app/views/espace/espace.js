import React from "react"
import {
  Row, Col, Card
} from 'reactstrap';
import DataTableFilter from "./DataTableFilter"


class espace extends React.Component {

  render() {
    return (

      <div className="mt-5">
        <Row className="justify-content-center mb-5">
          <h1>
            Espace d'application
          </h1>
        </Row>
        <Row className="align-items-center">
          <Col lg={10}>
            <DataTableFilter />
          </Col>
          <Col lg={2}>
            <Card>
              <a
                href='/dashboard/apps'
                id="buyButton"
                className="btn btn-primary"
              >Mes Applications
              </a>
            </Card>

          </Col>
        </Row>
      </div>
    )


  }
}

export default espace
