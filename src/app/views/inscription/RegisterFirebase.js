import React from "react"
import { Form, FormGroup, Input, Label, Col, Row } from "reactstrap"

import { connect } from "react-redux"
import { signupWithFirebase } from "../../@core/redux/actions/auth/registerActions"

class RegisterFirebase extends React.Component {
  state = {
    email: "",
    password: "",
    name: "",
    confirmPass: ""
  }

  handleRegister = e => {
    e.preventDefault()
    this.props.signupWithFirebase(
      this.state.email,
      this.state.password,
      this.state.name
    )
  }

  render() {
    return (
      <Form className="mt-0">
        <Row>
          <Col md="6" sm="12">
            <FormGroup >
              <Label for="nameVertical">Nom</Label>
              <Input
                type="text"
                name="Nom"
                id="nameVertical"
                placeholder="Nom"
              />

            </FormGroup>
          </Col>
          <Col md="6" sm="12">
            <FormGroup >
              <Label for="lastNameMulti">Prenoms</Label>
              <Input
                type="text"
                name="Prenoms"
                id="lastNameMulti"
                placeholder="Prenoms"
              />

            </FormGroup>
          </Col>
          <Col md="6" sm="12">
            <Label for="CountryMulti">E-mail</Label>
            <FormGroup >
              <Input
                type="email"
                name="email"
                id="CountryMulti"
                placeholder="Email"
              />
            </FormGroup>
          </Col>
          <Col md="6" sm="12">
            <FormGroup >
              <Label for="cityMulti">Contact</Label>
              <Input
                type="number"
                name="Username"
                id="cityMulti"
                placeholder="Contact"
              />
            </FormGroup>
          </Col>

        </Row>
        <Row>
          <Col md="6" sm="12">
            <Label for="passwordMulti">Adresse</Label>
            <FormGroup >
              <Input
                type="text"
                name="name"
                id="Adress"
                placeholder="Adresse"
              />
            </FormGroup>
          </Col>
          <Col md="6" sm="12">
            <Label for="passwordMulti">Pays</Label>
            <FormGroup >
              <Input
                type="text"
                name="Country"
                id="Country"
                placeholder="Pays"
              />
            </FormGroup>
          </Col>
        </Row>
        <div className="d-flex justify-content-between">
          <a href='/dashboard/auth' id="buyButton" className="btn btn-primary"> S'inscrire</a>
        </div>
      </Form>

    )
  }
}
const mapStateToProps = state => {
  return {
    values: state.auth.register
  }
}
export default connect(mapStateToProps, { signupWithFirebase })(
  RegisterFirebase
)
