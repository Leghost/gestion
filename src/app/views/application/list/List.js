import React from "react"
import { Row, Col } from "reactstrap"
import Breadcrumbs from "../../../@vuexy/breadCrumbs/BreadCrumb"
import ListViewConfig from "../../../@vuexy/dataList/DataListConfig"
import queryString from "query-string"


class Application extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Breadcrumbs
          breadCrumbTitle="List View"
          breadCrumbParent="Data List"
          breadCrumbActive="List View"
        />
        <Row>
          <Col sm="12">
            <ListViewConfig parsedFilter={queryString.parse(this.props.location.search)} />
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

export default Application
