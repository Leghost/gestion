import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row
} from "reactstrap";
import {Mail, Package, Server, Shield} from "react-feather";


class New extends React.Component {
  render() {
    return (
      <div className={'mt-5'}>
        <Row className={'mr-1 ml-1'}>
          <Col lg={5} className={'mt-2'}>
            <Row className="align-items-center">
              <Col lg={{ span: 3, offset: 4 }} md={6}>
                <div className="title-heading mt-4">
                  <h1 className="heading mb-2">Application</h1>
                  <p className="para-desc text-muted">ged.com vous permet d'effectuer en toute simplicité :</p>

                </div>
              </Col>
            </Row>
          </Col>
          <Col lg={7}>
            <div>
              <Card>
                <CardHeader>
                  <CardTitle>Application</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row>
                      <Col sm="12">
                        <Label for="nameVerticalIcons">Nom de l'application</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="text"
                            name="name"
                            id="nameVerticalIcons"
                            placeholder="Nom de l'application"
                          />
                          <div className="form-control-position">
                            <Package size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col sm="12">
                        <Label for="nameVerticalIcons">Code d'application</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="text"
                            name="name"
                            id="nameVerticalIcons"
                            placeholder="Url(s) d'origne"
                          />
                          <div className="form-control-position">
                            <Server size={15}/>
                          </div>
                        </FormGroup>
                      </Col>

                      <Col sm="12">
                        <FormGroup className="has-icon-left position-relative">
                          <Button.Ripple
                            color="primary"
                            type="submit"
                            className="mr-1 mb-1"
                            onClick={e => e.preventDefault()}
                          >
                            Créer
                          </Button.Ripple>
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

export default New;
