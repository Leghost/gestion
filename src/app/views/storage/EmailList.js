import React from "react"
import {
  FormGroup,
  Input,

  Row,


} from "reactstrap"
import {
  Search,

  Menu
} from "react-feather"
import PerfectScrollbar from "react-perfect-scrollbar"
import { connect } from "react-redux"
import {
  getEmails,
  StarEmail,
  searchMail,
  moveMail,
  selectMail,
  selectAllMails,
  deselectAllMails,
  unreadMails,
  setLabel
} from "../../@core/redux/actions/email/index"
import EmailDetails from "./EmailDetails"




class EmailList extends React.Component {
  static getDerivedStateFromProps(props, state) {
    if (props.email.params !== state.currentFilter) {
      return {
        mails: props.email.mails
      }
    }
    // Return null if the state hasn't changed
    return null
  }

  state = {
    mails: [],
    emailDetailsVisibility: false,
    selectAll: false,
    currentEmail: [],
    value: "",
    currentFilter: this.props.routerProps.match.params.filter
  }

  async componentDidMount() {
    await this.props.getEmails(this.props.routerProps.match.params)
    this.setState({
      mails: this.props.email.mails
    })
  }

  handleEmailDetails = (status, mail) => {
    if (status === "open")
      this.setState({ emailDetailsVisibility: true, currentEmail: mail });
    else this.setState({ emailDetailsVisibility: false });
  };

  handleNextMail = () => {
    let mails = this.state.mails
    if (mails.length) {
      let getIndex = mails.find(i => i.id === this.state.currentEmail.id)
      let currentEmail = mails[mails.indexOf(getIndex) + 1]

      if (currentEmail !== undefined) {
        this.setState({ currentEmail })
      }
    }
  }

  handlePreviousMail = () => {
    let mails = this.state.mails
    if (mails.length) {
      let getIndex = mails.find(i => i.id === this.state.currentEmail.id)
      let currentEmail = mails[mails.indexOf(getIndex) - 1]

      if (currentEmail !== undefined) {
        this.setState({ currentEmail })
      }
    }
  }

  handleOnChange = e => {
    this.setState({ value: e.target.value })
    this.props.searchMail(e.target.value)
  }

  render() {
    const { mails, value } = this.state
    const mailsArr = value.length ? this.props.email.filteredMails : mails
    const renderMails =
      mailsArr.length > 0 ? (
        mailsArr.map(mail => {
          return (
            <h1>s</h1>
          )
        })
      ) : (
          <div className="no-results show">
            <h5>No Items Found</h5>
          </div>
        )

    return (
      <div className="content-right">
        <div className="email-app-area">
          <div className="email-app-list-wrapper">
            <div className="email-app-list">
              <div className="app-fixed-search">
                <div
                  className="d-lg-none sidebar-toggle"
                  onClick={() => this.props.mainSidebar(true)}
                >
                  <Menu size={24} />
                </div>
                <FormGroup className="position-relative has-icon-left m-0 d-inline-block d-lg-block">
                  <Input
                    placeholder="Recherche"
                    onChange={e => {
                      this.handleOnChange(e)
                    }}
                    value={value}
                  />
                  <div className="form-control-position">
                    <Search size={15} />
                  </div>
                </FormGroup>
              </div>


            </div>
            <PerfectScrollbar
              className="email-user-list list-group"
              options={{
                wheelPropagation: false
              }}
            >
              <Row className="users-list-wrapper media-list">{renderMails}</Row>
            </PerfectScrollbar>
          </div>
        </div>
        <EmailDetails
          handleEmailDetails={this.handleEmailDetails}
          currentStatus={this.state.emailDetailsVisibility}
          currentEmail={this.state.currentEmail}
          toggleStarred={this.props.StarEmail}
          setLabel={this.props.setLabel}
          unreadMails={this.props.unreadMails}
          currentParam={this.props.routerProps.match.params}
          moveMail={this.props.moveMail}
          handleNextMail={this.handleNextMail}
          handlePreviousMail={this.handlePreviousMail}
        />
      </div>

    )
  }
}

const mapStateToProps = state => {
  return {
    email: state.emailApp.mails,
    starred: state.emailApp.starred
  }
}
export default connect(mapStateToProps, {
  getEmails,
  StarEmail,
  searchMail,
  moveMail,
  selectMail,
  selectAllMails,
  deselectAllMails,
  unreadMails,
  setLabel
})(EmailList)
