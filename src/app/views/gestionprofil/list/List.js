import React from "react"
import { Card, CardBody, CardHeader, CardTitle, Input,Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,FormGroup,Label  } from "reactstrap"
import { Search,Edit,Trash } from "react-feather"
import DataTable from "react-data-table-component"
import ModalForm from './../Ajout';

const CustomHeader = props => {
  return (
   <div className="d-flex flex-wrap justify-content-between">
      <ModalForm/>
      <div className="position-relative has-icon-left mb-1 ml-5">
        <Input value={props.value} onChange={e => props.handleFilter(e)} />
        <div className="form-control-position">
          <Search size="15" />
        </div>
      </div>
    </div>
  )
}
const ActionsComponent = props => {
  return (
    <div className="data-list-action">
      <Edit
        className="cursor-pointer mr-1"
        size={20}
        onClick={() => {
          return props.currentData(props.row)
        }}
      />
      <Trash
        className="cursor-pointer"
        size={20}
        onClick={() => {
          props.deleteRow(props.row)
        }}
      />
    </div>
  )
}
class DataTableFilter extends React.Component {


  state = {
    columns: [
      {
        name: "Id",
        selector: "id",
        sortable: true
      },
      {
        name: "Nom",
        selector: "nom",
        sortable: true
      },
      {
        name: "Prenoms",
        selector: "prenoms",
        sortable: true
      },
      {
        name: "Email",
        selector: "email",
        sortable: true
      },
      {
        name: "Actions",
        sortable: true,
        cell: row => (
          <ActionsComponent
            row={row}
            getData={this.props.getData}
            parsedFilter={this.props.parsedFilter}
            currentData={this.handleCurrentData}
            deleteRow={this.handleDelete}
          />
        )
      }
    ],
    data: [
      {
        id: 1,
        nom: "Alyss",
        prenoms: "Lillecrop",
        email: "alillecrop0@twitpic.com",
        gender: "Female"
      },
      {
        id: 2,
        nom: "Shep",
        prenoms: "Pentlow",
        email: "spentlow1@home.pl",
        gender: "Male"
      },
      {
        id: 3,
        nom: "Gasper",
        prenoms: "Morley",
        email: "gmorley2@chronoengine.com",
        gender: "Male"
      },
      {
        id: 4,
        nom: "Phaedra",
        prenoms: "Jerrard",
        email: "pjerrard3@blogs.com",
        gender: "Female"
      },
      {
        id: 5,
        nom: "Conn",
        prenoms: "Plose",
        email: "cplose4@geocities.com",
        gender: "Male"
      },
      {
        id: 6,
        nom: "Tootsie",
        prenoms: "Brandsma",
        email: "tbrandsma5@theatlantic.com",
        gender: "Female"
      },
      {
        id: 7,
        nom: "Sibley",
        prenoms: "Bum",
        email: "sbum6@sourceforge.net",
        gender: "Female"
      },
      {
        id: 8,
        nom: "Kristoffer",
        prenoms: "Thew",
        email: "kthew7@amazon.com",
        gender: "Male"
      },
      {
        id: 9,
        nom: "Fay",
        prenoms: "Hasard",
        email: "fhasard8@java.com",
        gender: "Female"
      },
      {
        id: 10,
        nom: "Tabby",
        prenoms: "Abercrombie",
        email: "tabercrombie9@statcounter.com",
        gender: "Female"
      }
    ],
    value: "",
    filteredData: []
  }

  handleFilter = e => {
    let value = e.target.value
    let data = this.state.data
    let filteredData = this.state.filteredData
    this.setState({ value })

    if (value.length) {
      filteredData = data.filter(item => {
        let startsWithCondition =
          item.nom.toLowerCase().startsWith(value.toLowerCase()) ||
          item.prenoms.toLowerCase().startsWith(value.toLowerCase()) ||
          item.email.toLowerCase().startsWith(value.toLowerCase()) ||
          item.gender.toLowerCase().startsWith(value.toLowerCase()) ||
          item.id
            .toString()
            .toLowerCase()
            .startsWith(value.toLowerCase())
        let includesCondition =
          item.nom.toLowerCase().includes(value.toLowerCase()) ||
          item.prenoms.toLowerCase().includes(value.toLowerCase()) ||
          item.email.toLowerCase().includes(value.toLowerCase()) ||
          item.gender.toLowerCase().includes(value.toLowerCase()) ||
          item.id
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())

        if (startsWithCondition) {
          return startsWithCondition
        } else if (!startsWithCondition && includesCondition) {
          return includesCondition
        } else return null
      })
      this.setState({ filteredData })
    }
  }

  render() {
    let { columns, data, value, filteredData } = this.state
    return (
       <div className="m-0  justify-content-center">
        <Card>
          <CardHeader>
            <CardTitle>Liste des acces</CardTitle>
          </CardHeader>
          <CardBody>
            <DataTable
              data={value.length ? filteredData : data}
              columns={columns}
              fixedHeader
              fixedHeaderScrollHeight="300px"
              noHeader
              subHeader
              subHeaderComponent={
                <CustomHeader value={value} handleFilter={this.handleFilter} />
              }
            />
          </CardBody>
        </Card>

        <Modal
        isOpen={this.state.modal}
        toggle={this.toggleModal}
        className="modal-dialog-centered"
      >
        <ModalHeader toggle={this.toggleModal}>
          Login Form
        </ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label for="email">Email:</Label>
            <Input
              type="email"
              id="email"
              placeholder="Email Address"
            />
          </FormGroup>
          <FormGroup>
            <Label for="password">Password:</Label>
            <Input
              type="password"
              id="password"
              placeholder="Password"
            />
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.toggleModal}>
            Login
          </Button>{" "}
        </ModalFooter>
      </Modal>
    </div>

    )
  }
}

export default DataTableFilter






