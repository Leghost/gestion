import React, { Suspense, lazy } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Layout } from "./app/@core/utility/context/Layout";
import * as serviceWorker from "./serviceWorker";
import { store } from "./app/@core/redux/storeConfig/store";
import Spinner from "./app/@vuexy/spinner/Fallback-spinner";
import "./index.scss";
import "./app/@core/@fake-db";
import { Route, Router, Switch } from "react-router";
import { history } from "./history";

const LazyApp = lazy(() => import("./app/App"));
const LandingApp = lazy(() => import("./landing/App"));


// configureDatabase()
ReactDOM.render(
  <Provider store={store}>
    <Suspense fallback={<Spinner />}>
      <Layout>
        <Router history={history}>
          <Switch>
            <Route path={'/dashboard*'}>
              <LazyApp />
            </Route>
            <Route exact path={'/*'}>
              <LandingApp />
            </Route>
          </Switch>
        </Router>
      </Layout>
    </Suspense>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more landing service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
